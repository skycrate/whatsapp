const { app, BrowserWindow, nativeImage, Tray } = require('electron');
const icon = __dirname + '/icon.png';

function init () {
	const win = new BrowserWindow({
		width: 800,
		height: 920,
		icon: nativeImage.createFromPath(icon),
		// show: false,
		webPreferences: {
			nodeIntegration: false
		}
	});
	win.setTitle("WhatsApp Messenger - Linux");
	win.setMenuBarVisibility(false);
	win.loadURL('https://web.whatsapp.com', {
		userAgent: "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36"
	});
};

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin')
		app.quit();
});

app.on('activate', () => {
	if (BrowserWindow.getAllWindows().length === 0)
		init();
});

app.on('ready', init);
