# whatsapp

Desktop Application for WhatsApp.

![screenshot](./peek.png)

Contains built application for linux by using Neutralino. Currently the `SETUP`
and `INSTALL` files assume using __Electron__, but __electron__ embeds too old of
a browser and so it doesn't work for whatsapp specifically (but works if you
modify main.js to point to _other_ URLs).

### UPDATE (Electron issues resolved):
__Electron__ now working (spoofed `userAgent` string to trick Whatsapp domain
into thinking we're using the newest version of chrome). Do the following:

```
./SETUP && sudo ./INSTALL
```

and Bob's yer uncle! The main.js file can be modified at any time... no need to
do any "builds"... and program can be accessed from your application launcher and
added to your dock or panel.

### Using Neutralino instead of Electron...

If you wish to install the neutralino Linux version, you will need to modify the
`INSTALL` file:

```
Exec=$dir/whatsapp-linux
```

prior to running `sudo ./INSTALL`

If you wish to modify the working `whatsapp-linux` application, the package-old.json
file will need to be renamed to package.json, and your package-lock.json file deleted.
Then, after modifying your source files in `app/` and `src/`, you can run:

```
./BUILD && sudo ./INSTALL
```

Now you can access the application in your system menu.
