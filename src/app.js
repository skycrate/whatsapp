// @ts-nocheck
//import {AppLib} from './app-core/lib'; TODO: adopt this style!
//import './mycss.css';
//import './mycss2.css';
// We can import css here... but why? Other than for component layouts! (i.e. default built-in "flavours" for controls. yum.)

Neutralino.init({
	load: function() {
		// APPLICATION ENTRY POINT:
		//alert(NL_NAME + " is running on port " + NL_PORT + " inside " + NL_OS + "<br/><br/>" + "<span>v" + NL_VERSION + "</span>");
		window.location.replace('https://web.whatsapp.com/');
	}
});
